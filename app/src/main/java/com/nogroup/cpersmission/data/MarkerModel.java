package com.nogroup.cpersmission.data;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class MarkerModel implements ClusterItem {

    private  LatLng mPosition;
    private  String mTitle;
    private  String mSnippet;
    private int id;


    public MarkerModel(LatLng mPosition, String mTitle, String mSnippet, int id) {
        this.mPosition = mPosition;
        this.mTitle = mTitle;
        this.mSnippet = mSnippet;
        this.id = id;
    }

    public MarkerModel(double lat, double lng) {
        mPosition = new LatLng(lat, lng);
    }

    public MarkerModel(double lat, double lng, String title, String snippet) {
        mPosition = new LatLng(lat, lng);
        mTitle = title;
        mSnippet = snippet;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public String getSnippet() {
        return mSnippet;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "MarkerModel{" +
                "mPosition=" + mPosition +
                ", mTitle='" + mTitle + '\'' +
                ", mSnippet='" + mSnippet + '\'' +
                ", id=" + id +
                '}';
    }
}