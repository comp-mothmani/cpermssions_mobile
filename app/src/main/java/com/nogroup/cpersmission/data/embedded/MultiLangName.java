package com.nogroup.cpersmission.data.embedded;

import android.content.Context;

import java.util.Locale;

public class MultiLangName {

    private String arName;
    private String frName;
    private String enName;

    public MultiLangName() {
        super();
    }

    public MultiLangName(String arName, String frName, String enName) {
        super();
        this.arName = arName;
        this.frName = frName;
        this.enName = enName;
    }

    public String getArName() {
        return arName;
    }

    public void setArName(String arName) {
        this.arName = arName;
    }

    public String getFrName() {
        return frName;
    }

    public void setFrName(String frName) {
        this.frName = frName;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public String getLocal(Context ctxt) {
        //Locale current = ctxt.getResources().getConfiguration().locale;
        String lang = Locale.getDefault().getDisplayLanguage();
        if (lang.equals("ar")) {
            return arName;
        } else if (lang.equals("fr")) {
            return frName;
        } else if (lang.equals("en")) {
            return enName;
        }

        return "";
    }

    @Override
    public String toString() {
        return "MultiLangName{" +
                "arName='" + arName + '\'' +
                ", frName='" + frName + '\'' +
                ", enName='" + enName + '\'' +
                '}';
    }
}