package com.nogroup.cpersmission.data.entities;

import com.nogroup.cpersmission.data.embedded.MultiLangName;
import com.nogroup.cpersmission.data.embedded.VFile;

import java.util.Date;
import java.util.HashMap;

public class User {

	private Integer id;

    private MultiLangName name;

    private String email;

    private String hierarchy;
	
    private Date dCreated;
	
    public VFile picture;
	
    private HashMap<String, Object> other = new HashMap<String, Object>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public MultiLangName getName() {
		return name;
	}

	public void setName(MultiLangName name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getdCreated() {
		return dCreated;
	}

	public void setdCreated(Date dCreated) {
		this.dCreated = dCreated;
	}

	public VFile getPicture() {
		return picture;
	}

	public void setPicture(VFile picture) {
		this.picture = picture;
	}

	public HashMap<String, Object> getOther() {
		return other;
	}

	public void setOther(HashMap<String, Object> other) {
		this.other = other;
	}

	public String getHierarchy() {
		return hierarchy;
	}

	public void setHierarchy(String hierarchy) {
		this.hierarchy = hierarchy;
	}
	
}