package com.nogroup.cpersmission.prez;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

import com.nogroup.cpersmission.R;

import java.util.Locale;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener {

    ImageButton arab_btn;
    ImageButton french_btn;
    ImageButton english_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        /***
         *Set Language Settings
         */
        Configuration config = new Configuration();
        config.locale = Locale.FRENCH;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());


        initViews();

    }

    public void initViews() {
        arab_btn = findViewById(R.id.arabic_lang);
        arab_btn.setOnClickListener(this);
        french_btn = findViewById(R.id.french_lang);
        french_btn.setOnClickListener(this);
        english_btn = findViewById(R.id.english_lang);
        english_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.arabic_lang) {
            initLanguage("ar");
        } else if (v.getId() == R.id.french_lang) {
            initLanguage("fr");
        } else if (v.getId() == R.id.english_lang) {
            initLanguage("en");
        }

    }

    public void initLanguage(String language) {

        switch (language) {
            case "ar":
                Locale locale = new Locale("ar");
                Locale.setDefault(locale);
                Configuration config_ar = new Configuration();
                config_ar.locale = locale;
                getBaseContext().getResources().updateConfiguration(config_ar,
                        getBaseContext().getResources().getDisplayMetrics());
                break;
            case "en": {
                Configuration config = new Configuration();
                config.locale = Locale.ENGLISH;
                getResources().updateConfiguration(config, getResources().getDisplayMetrics());

                break;
            }
            case "fr": {
                Configuration config = new Configuration();
                config.locale = Locale.FRENCH;
                getResources().updateConfiguration(config, getResources().getDisplayMetrics());

                break;
            }
        }

        Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
        startActivity(intent);

    }
}
