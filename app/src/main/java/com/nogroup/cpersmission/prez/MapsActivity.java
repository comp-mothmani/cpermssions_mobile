package com.nogroup.cpersmission.prez;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.nogroup.cpersmission.AppContext;
import com.nogroup.cpersmission.business.PermissionUtils;
import com.nogroup.cpersmission.data.MarkerModel;
import com.nogroup.cpersmission.R;
import com.nogroup.cpersmission.data.embedded.MultiLangName;
import com.nogroup.cpersmission.data.embedded.VPoint;
import com.nogroup.cpersmission.data.entities.Dwelling;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_HYBRID;
import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    private Button btn;
    private boolean clicked = false;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private int mSelectedStyleId = R.string.style_label_default;
    private static final String SELECTED_STYLE = "selected_style";
    protected SlidingUpPanelLayout spl;
    private int mStyleIds[] = {
            R.string.style_label_Hybrid,
            R.string.style_label_Normal,
            R.string.style_label_retro,
            R.string.style_label_night,
            R.string.style_label_dark,
            R.string.style_label_aubergine,
            R.string.style_label_grayscale, +
            R.string.style_label_no_pois_no_transit,
            R.string.style_label_default,
    };
    private ImageButton permis_1;
    private ImageButton permis_2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);


        /***
         * ActionBar color
         */
        getSupportActionBar().setBackgroundDrawable(
                new ColorDrawable(Color.parseColor("#0277BD")));


        /***
         * Set Map Style by default
         */
        if (savedInstanceState != null) {
            mSelectedStyleId = savedInstanceState.getInt(SELECTED_STYLE);
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        initViews();


    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        mMap.setOnMarkerClickListener(this);


        setUpClusterManager(mMap);
    }

    public void initViews(){

        spl = findViewById(R.id.spl);
        spl.setEnabled(false);


        permis_1 = findViewById(R.id.permis1);
        permis_1.setOnClickListener(this);

        permis_2 = findViewById(R.id.permis2);
        permis_2.setOnClickListener(this);

    }

    public void initSlidingPanel(Marker marker) {
        spl.setEnabled(true);
        spl.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);

    }

    private void setUpClusterManager(GoogleMap googleMap) {
        ClusterManager<MarkerModel> clusterManager = new ClusterManager(this, googleMap);  // 3

        final CustomClusterRenderer renderer = new CustomClusterRenderer(this, googleMap, clusterManager);
        clusterManager.setRenderer(renderer);

        googleMap.setOnCameraIdleListener(clusterManager);
        List<MarkerModel> items = getItems();
        clusterManager.addItems(items);  // 4
        clusterManager.cluster();  // 5



    }

    private List<MarkerModel> getItems() {

        List<MarkerModel> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {

            for (int j = 0; j < 10; j++) {
                Dwelling dwelling = new Dwelling();
                dwelling.setAddress(new MultiLangName("ar"+i+" "+j,"fr"+i+" "+j,"en"+i+" "+j));
                dwelling.setOwner("mahdi" + i+ " " + j);
                dwelling.setCoordinates(new VPoint(-34 + i * 0.1, 151 + j * 0.1));
                dwelling.setId(i);
                list.add(dwelling.toMarkerModel(getApplicationContext()));

                Log.e("MARKER",dwelling.toString());
            }
        }
        return list;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        initSlidingPanel(marker);
        Toast.makeText(getApplicationContext(), marker.getTitle() + "CLICKED", Toast.LENGTH_LONG).show();
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /***
         * Set Action to ActionBar items
         */
        int item1 = item.getItemId();
         if (item1 == R.id.map_type) {

            showStylesDialog();

        } else if (item1 == R.id.MyLocation) {

            initLocationActionBarItem();

        } else if (item1 == R.id.infraction) {
             addNewInfractionAlert();
         } else {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            return super.onOptionsItemSelected(item);
        }

        return true;

    }

    public void initLocationActionBarItem() {

        if (!clicked) {
            updateMyLocation();
            clicked = true;
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(false);
            clicked = false;
        }

    }

    private void updateMyLocation() {
        if (!checkReady()) {
            return;
        }

        // Enable the location layer. Request the location permission if needed.
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            // Uncheck the box until the layer has been enabled and request missing permission.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, false);
        }
    }

    private boolean checkReady() {
        if (mMap == null) {
            Toast.makeText(this, R.string.map_not_ready, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    /**
     * Creates a {@link MapStyleOptions} object via loadRawResourceStyle() (or via the
     * constructor with a JSON String), then sets it on the {@link GoogleMap} instance,
     * via the setMapStyle() method.
     */
    private void setSelectedStyle() {
        MapStyleOptions style = null;
        switch (mSelectedStyleId) {
            case R.string.style_label_Hybrid:
                // Sets the retro style via raw resource JSON.
                mMap.setMapType(MAP_TYPE_HYBRID);
                break;

            case R.string.style_label_Normal:
                // Sets the retro style via raw resource JSON.
                mMap.setMapType(MAP_TYPE_NORMAL);
                break;

            case R.string.style_label_retro:
                if (mMap.getMapType() == MAP_TYPE_HYBRID) {
                    Toast.makeText(getApplicationContext(), "You cannot overcome Hybrid layer, please change map type to Normal layer, to choose other map types", Toast.LENGTH_LONG).show();
                } else {
                    // Sets the retro style via raw resource JSON.
                    style = MapStyleOptions.loadRawResourceStyle(this, R.raw.mapstyle_retro);
                }
                break;

            case R.string.style_label_night:
                if (mMap.getMapType() == MAP_TYPE_HYBRID) {
                    Toast.makeText(getApplicationContext(), "You cannot overcome Hybrid layer, please change map type to Normal layer, to choose other map types", Toast.LENGTH_LONG).show();
                } else {
                    // Sets the night style via raw resource JSON.
                    style = MapStyleOptions.loadRawResourceStyle(this, R.raw.mapstyle_night);
                }
                break;

            case R.string.style_label_dark:
                if (mMap.getMapType() == MAP_TYPE_HYBRID) {
                    Toast.makeText(getApplicationContext(), "You cannot overcome Hybrid layer, please change map type to Normal layer, to choose other map types", Toast.LENGTH_LONG).show();
                } else {
                    // Sets the night style via raw resource JSON.
                    style = MapStyleOptions.loadRawResourceStyle(this, R.raw.dark);
                }
                break;

            case R.string.style_label_aubergine:
                if (mMap.getMapType() == MAP_TYPE_HYBRID) {
                    Toast.makeText(getApplicationContext(), "You cannot overcome Hybrid layer, please change map type to Normal layer, to choose other map types", Toast.LENGTH_LONG).show();
                } else {
                    // Sets the night style via raw resource JSON.
                    style = MapStyleOptions.loadRawResourceStyle(this, R.raw.aubergine);
                }
                break;

            case R.string.style_label_grayscale:
                if (mMap.getMapType() == MAP_TYPE_HYBRID) {
                    Toast.makeText(getApplicationContext(), "You cannot overcome Hybrid layer, please change map type to Normal layer, to choose other map types", Toast.LENGTH_LONG).show();
                } else {
                    // Sets the grayscale style via raw resource JSON.
                    style = MapStyleOptions.loadRawResourceStyle(this, R.raw.mapstyle_grayscale);
                }
                break;

            case R.string.style_label_no_pois_no_transit:
                if (mMap.getMapType() == MAP_TYPE_HYBRID) {
                    Toast.makeText(getApplicationContext(), "You cannot overcome Hybrid layer, please change map type to Normal layer, to choose other map types", Toast.LENGTH_LONG).show();
                } else {
                    // Sets the no POIs or transit style via JSON string.
                    style = new MapStyleOptions("[" +
                            "  {" +
                            "    \"featureType\":\"poi.business\"," +
                            "    \"elementType\":\"all\"," +
                            "    \"stylers\":[" +
                            "      {" +
                            "        \"visibility\":\"off\"" +
                            "      }" +
                            "    ]" +
                            "  }," +
                            "  {" +
                            "    \"featureType\":\"transit\"," +
                            "    \"elementType\":\"all\"," +
                            "    \"stylers\":[" +
                            "      {" +
                            "        \"visibility\":\"off\"" +
                            "      }" +
                            "    ]" +
                            "  }" +
                            "]");
                }

                break;
            case R.string.style_label_default:
                // Removes previously set style, by setting it to null.
                style = null;
                break;
            default:
                return;
        }
        mMap.setMapStyle(style);
    }

    /**
     * Shows a dialog listing the styles to choose from, and applies the selected
     * style when chosen.
     */
    private void showStylesDialog() {
        // mStyleIds stores each style's resource ID, and we extract the names here, rather
        // than using an XML array resource which AlertDialog.Builder.setItems() can also
        // accept. We do this since using an array resource would mean we would not have
        // constant values we can switch/case on, when choosing which style to apply.
        List<String> styleNames = new ArrayList<>();
        for (int style : mStyleIds) {
            styleNames.add(getString(style));
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //builder.setTitle(getString(R.string.style_choose));
        builder.setTitle("Map Type: " + getString(mSelectedStyleId));
        builder.setItems(styleNames.toArray(new CharSequence[styleNames.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mSelectedStyleId = mStyleIds[which];
                        String msg = getString(R.string.style_set_to, getString(mSelectedStyleId));
                        AppContext.MAP_TYPE = getString(mSelectedStyleId);
                        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
                        setSelectedStyle();
                    }
                });
        builder.show();
    }


    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.permis1) {
            String pdf_url ="https://drive.google.com/file/d/19OyejWE2oK7xiObrOkkhUVrWdxUUAey7/view";
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pdf_url));
            startActivity(browserIntent);
        }else if(v.getId() == R.id.permis2) {
            String pdf_url ="https://drive.google.com/file/d/19OyejWE2oK7xiObrOkkhUVrWdxUUAey7/view";
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pdf_url));
            startActivity(browserIntent);
        }
    }


    public class CustomClusterRenderer extends DefaultClusterRenderer<MarkerModel> {

        private final Context mContext;

        public CustomClusterRenderer(Context context, GoogleMap map,
                                     ClusterManager<MarkerModel> clusterManager) {
            super(context, map, clusterManager);

            mContext = context;
        }

        @Override protected void onBeforeClusterItemRendered(MarkerModel item, MarkerOptions markerOptions) {

            int heights = 40;
            int widths = 50;
            BitmapDrawable bitmapdrawcs = (BitmapDrawable) getResources().getDrawable(R.drawable.csite);
            Bitmap bcs = bitmapdrawcs.getBitmap();
            Bitmap csicon = Bitmap.createScaledBitmap(bcs, widths, heights, false);
            BitmapDescriptor bit = BitmapDescriptorFactory.fromBitmap(csicon);
            markerOptions.icon(bit);
        }
    }

    public void addNewInfractionAlert(){

        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = inflater.inflate(R.layout.add_new_infraction, null);
        builder.setView(view);

        /***
         * custom title for alert dialogue
         */
        final View customTitleView = inflater.inflate(R.layout.custom_title, null);
        TextView title = customTitleView.findViewById(R.id.customtitle);
        ImageView img = customTitleView.findViewById(R.id.imageView3);
        title.setText("Add New Infraction");
        img.setImageResource(R.drawable.language);
        builder.setCustomTitle(customTitleView);

        /*Button btn_position = view.findViewById(R.id.button_position);
        final TextView mark_lat = view.findViewById(R.id.mark_lat);
        final TextView mark_lon = view.findViewById(R.id.mark_lon);
        final EditText building_name = view.findViewById(R.id.buildingname);*/



        builder.setPositiveButton("Save", null);

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

}
