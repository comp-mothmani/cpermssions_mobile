package com.nogroup.cpersmission.data.embedded;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

public class VPoint implements Serializable {
	/**
	 * @author medzied
	 */
	private static final long serialVersionUID = 1L;
	
	private double longitude;
	private double latitude;
	
	public VPoint() {
	}

	public VPoint( double latitude,double longitude) {
		this.longitude = longitude ;
		this.latitude = latitude ;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public LatLng toLatLon() {
		return new LatLng(latitude, longitude);
	}

	@Override
	public String toString() {
		return "VPoint{" +
				"longitude=" + longitude +
				", latitude=" + latitude +
				'}';
	}
}