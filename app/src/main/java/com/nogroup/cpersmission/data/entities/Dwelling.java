package com.nogroup.cpersmission.data.entities;


import android.content.Context;
import com.nogroup.cpersmission.data.MarkerModel;
import com.nogroup.cpersmission.data.embedded.MultiLangName;
import com.nogroup.cpersmission.data.embedded.VPoint;
import java.util.HashMap;

public class Dwelling {

	private Integer id;
	
    private String owner;
	
    private VPoint coordinates;
    
    private MultiLangName address;

    private HashMap<String, Object> other = new HashMap<String, Object>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public VPoint getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(VPoint coordinates) {
		this.coordinates = coordinates;
	}

	public MultiLangName getAddress() {
		return address;
	}

	public void setAddress(MultiLangName address) {
		this.address = address;
	}

	public HashMap<String, Object> getOther() {
		return other;
	}

	public void setOther(HashMap<String, Object> other) {
		this.other = other;
	}

	public MarkerModel toMarkerModel(Context ctxt) {
		return new MarkerModel(coordinates.toLatLon(), address.getLocal(ctxt), owner,id);
	}

	@Override
	public String toString() {
		return "Dwelling{" +
				"id=" + id +
				", owner='" + owner + '\'' +
				", coordinates=" + coordinates +
				", address=" + address +
				", other=" + other +
				'}';
	}


}