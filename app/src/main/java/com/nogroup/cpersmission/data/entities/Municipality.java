package com.nogroup.cpersmission.data.entities;

import com.nogroup.cpersmission.data.embedded.MultiLangName;
import com.nogroup.cpersmission.data.embedded.VPolygon;

import java.util.Date;
import java.util.HashMap;

public class Municipality {

	private Integer id;

    public MultiLangName name;

    private VPolygon coordinates;

    private Date dCreated;

    private HashMap<String, Object> other = new HashMap<String, Object>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public MultiLangName getName() {
		return name;
	}

	public void setName(MultiLangName name) {
		this.name = name;
	}

	public VPolygon getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(VPolygon coordinates) {
		this.coordinates = coordinates;
	}

	public Date getdCreated() {
		return dCreated;
	}

	public void setdCreated(Date dCreated) {
		this.dCreated = dCreated;
	}

	public HashMap<String, Object> getOther() {
		return other;
	}

	public void setOther(HashMap<String, Object> other) {
		this.other = other;
	}

	@Override
	public String toString() {
		return "Municipality [id=" + id + ", name=" + name + ", coordinates=" + coordinates + ", dCreated=" + dCreated
				+ ", other=" + other + "]";
	}
}